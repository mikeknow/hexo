這是利用 [Hexo](https://hexo.io) 在 GitLab Pages 上架設的範本。

關於更多 GitLab Pages 的資訊可 [點我](https://pages.gitlab.io)，官方文件可參考 [這裡](https://docs.gitlab.com/ee/user/project/pages/)。

---

## Fork 這專案範本

直接點選右上角的 Fork 字樣，選擇您的帳戶，即可將它帶回家。

## 移除關聯

當您把它帶回家後，記得移除關聯，避免我這邊的修改連動到您的專案。
到左下角的 **Settings** > **General** > **Advanced**，
展開後選擇 **remove the forking relationship**

## 變更專案名稱，優化網址

如果沒做這修改，預設網址是 `https://您的帳號.gitlab.io/專案名稱`

到左下角的 **Settings** > **General** > **Advanced**，
展開後將您的專案名稱為 `您的帳號.gitlab.io`，如此一來，您的部落格網址就會變成 `https://您的帳號.gitlab.io`

## 更新維護部落格

在您電腦上維護修改本專案，可依照下列步驟:

1. 透過 [Github Desktop](https://desktop.github.com/) 下載本專案
1. 確認電腦已安裝 [Node.js](https://nodejs.org/en/)
1. 在命令提示字元輸入 `node -v`，如果顯示版本號，代表已經安裝，建議使用 10 以上版本。
1. 若無安裝，可到 [這裡](https://nodejs.org/en/) 下載並一直下一步進行安裝。
1. 安裝所有依賴的套件: `npm i`
1. 修改或新增文章內容
1. 本地編譯為靜態網頁: `hexo s`
1. 預覽您的部落格網頁: `http://localhost:4000`
1. 如沒問題，透過 [Github Desktop](https://desktop.github.com/) 上傳更新本專案，網頁就會自動更新。






