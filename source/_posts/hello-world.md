---
title: 歡迎使用 Hexo
---
歡迎使用 [Hexo](https://hexo.io/)! 這是預設的第一篇貼文. [點我](https://hexo.io/docs/) 查看更多資訊. 如果有任何使用上遇到的問題，可在 [這裡](https://hexo.io/docs/troubleshooting.html) 找到解決方案，或是直接在 [這裡](https://github.com/hexojs/hexo/issues) 詢問原作者。

## 快速使用方法

### 建立文章

``` bash
$ hexo new "My New Post"
```

更多資訊: [點我](https://hexo.io/docs/writing.html)

### 本地執行伺服器

``` bash
$ hexo server
```

更多資訊: [點我](https://hexo.io/docs/server.html)

### 產生靜態網頁

``` bash
$ hexo generate
```

更多資訊: [點我](https://hexo.io/docs/generating.html)

### 發布

``` bash
$ hexo deploy
```

更多資訊: [點我](https://hexo.io/docs/deployment.html)

翻譯: [Mike Chen](https://gitlab.com/mikeknow/)
